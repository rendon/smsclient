SMSClient
=========
A client for my another project [SMSServer](https://bitbucket.org/rendon/smsserver).

For more information see the project write up (in Spanish): [http://rendon.x10.mx/?p=833](http://rendon.x10.mx/?p=833).

License
=======
This work is under GPLv3.
