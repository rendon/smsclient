package com.inforscience.client;

import javax.swing.*;
import javax.swing.UIManager;
import java.awt.*;

public class Client extends JFrame {

    public static final Color GUI_ERROR_COLOR = new Color(183, 72, 60);
    private static final int GUI_WIDTH = 400;
    private static final int GUI_HEIGHT = 450;

    private SmsClient smsClient;

    public Client()
    {
        super("Cliente SMS");
        smsClient = new SmsClient("http", "192.168.1.242", 8080);
        initializeUI();
    }

    private void initializeUI()
    {
        try {
            String theme = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
            UIManager.setLookAndFeel(theme);
        } catch (Exception e) {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e2) {
            }
        }

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(GUI_WIDTH, GUI_HEIGHT);
        setLocationRelativeTo(null);
        setVisible(true);

        StatusBar statusBar = new StatusBar();
        new Thread(statusBar).start();
        InboxPanel inboxPanel = new InboxPanel(smsClient, statusBar);
        SentMessagePanel sentMessagePanel = new SentMessagePanel(smsClient, statusBar);
        NewSmsPanel newSmsPanel = new NewSmsPanel(smsClient, sentMessagePanel, statusBar);

        JPanel panel = new JPanel(new BorderLayout());
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Nuevo SMS", null,
                newSmsPanel,
                "Enviar nuevo SMS");

        tabbedPane.addTab("SMS Recibidos", null,
                inboxPanel,
                "Mensajes entrantes");

        tabbedPane.addTab("SMS Enviados", null,
                sentMessagePanel,
                "Mensajes enviados");


        panel.add(tabbedPane, BorderLayout.CENTER);
        add(panel, BorderLayout.CENTER);
        add(statusBar, BorderLayout.SOUTH);

        newSmsPanel.updateUI();
        Thread threadA = new Thread(newSmsPanel);
        Thread threadB = new Thread(sentMessagePanel);
        Thread threadC = new Thread(inboxPanel);

        threadA.start();
        threadB.start();
        threadC.start();
    }

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                new Client();
            }
        });
    }
}
