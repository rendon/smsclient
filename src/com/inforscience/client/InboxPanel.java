package com.inforscience.client;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;
import java.net.*;
import java.io.*;
import java.util.HashMap;

class InboxPanel extends JPanel implements Runnable {

    private JEditorPane messagePane;
    private SmsClient smsClient;
    private String htmlStyle;
    private String divHeaderStyle;
    private String divMessageStyle;

    private Vector<ReceivedMessage> receivedMessages;
    private StatusBar statusBar;
    private HashMap<String, String> contactsMap;

    public InboxPanel(SmsClient client, StatusBar bar)
    {
        smsClient = client;
        statusBar = bar;

        setLayout(new BorderLayout());

        messagePane = new JEditorPane();
        messagePane.setEditable(false);
        htmlStyle = "<html><body style=\"background-color: #1C1C1C;\">";
        divHeaderStyle = "<div style=\"padding: 5px; color: white; \">";
        divMessageStyle = "<div style=\"margin-top: 5px; padding: 5px; " +
                "color: white; background-color: #2E7EC4;\">";

        messagePane.setContentType("text/html");
        messagePane.setText(htmlStyle + "</body></html>");
        JScrollPane scrollPane = new JScrollPane(messagePane);
        add(scrollPane, BorderLayout.CENTER);

        SMSMonitor monitor = new SMSMonitor();
        Thread thread = new Thread(monitor);
        thread.start();
    }

    private String formatMessage(String from, String message)
    {
        String div = "<div style=\"margin: 5px; padding: 10px; " +
                "background-color: #3C3F41;\">";
        if (contactsMap.containsKey(from))
            from = contactsMap.get(from);
        div += divHeaderStyle + "<strong>De: </strong>" + from + "</div>";
        div += divMessageStyle + message + "</div>";
        div += "</div>";

        return div;
    }

    private void scrollToBottom()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                int length = messagePane.getDocument().getLength();
                if (length > 1)
                    messagePane.setCaretPosition(length - 1);
            }
        });
    }

    private void formatAllMessages()
    {
        String html = htmlStyle;
        for (int i = 0; i < receivedMessages.size(); i++) {
            String from = receivedMessages.get(i).getTransmitter();
            String message = receivedMessages.get(i).getText();
            String item = formatMessage(from, message);
            html += item;
        }
        html += "</body></html>";

        messagePane.setText(html);
        scrollToBottom();
    }

    public void addMessage(String from, String message)
    {
        receivedMessages.add(new ReceivedMessage(from, message));
        formatAllMessages();
    }

    private void loadContacts()
    {
        Vector<Contact> c = smsClient.getContactList();
        contactsMap = new HashMap<String, String>();

        for (int i = 0; i < c.size(); i++)
            contactsMap.put(c.get(i).getNumber(), c.get(i).getName());
    }

    @Override
    public void run()
    {
        while (true) {
            if (smsClient.testConnection()) {
                receivedMessages = smsClient.getReceivedMessages();
                if (receivedMessages != null) {
                    loadContacts();
                    formatAllMessages();
                    messagePane.updateUI();
                    break;
                }
            } else {
                messagePane.setText("<h1>Intentando contactar " +
                        "al servidor...</h1>");
            }

            sleep(2000);
        }
    }

    private void sleep(int milliseconds)
    {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    private class SMSMonitor implements Runnable {
        @Override
        public void run()
        {
            while (true) {
                String command = SmsClient.COMMAND_CHECK_FOR_NEW_MESSAGE;
                String parameters = "command=" + command;

                try {
                    String server = smsClient.getProtocol() + "://" +
                            smsClient.getServerName() + ":" +
                            smsClient.getPort();

                    URL url = new URL(server + "/?" + parameters);
                    HttpURLConnection http = (HttpURLConnection) url.openConnection();
                    http.getResponseCode();

                    InputStreamReader in = new InputStreamReader(http.getInputStream());
                    BufferedReader reader = new BufferedReader(in);

                    String line = reader.readLine();
                    if (line != null && line.equals(SmsClient.CODE_TRUE)) {
                        command = SmsClient.COMMAND_GET_LAST_MESSAGE;
                        parameters = "command=" + command;
                        url = new URL(server + "/?" + parameters);
                        http = (HttpURLConnection) url.openConnection();
                        http.getResponseCode();

                        in = new InputStreamReader(http.getInputStream());
                        reader = new BufferedReader(in);
                        line = reader.readLine();
                        String[] tokens = line.split(SmsClient.FIELD_SEPARATOR);

                        addMessage(tokens[0], tokens[1]);
                        statusBar.setStatus("Nuevo SMS.", StatusBar.CODE_SUCCESS);
                    }

                } catch (IOException ioe) {
                    System.out.println(ioe.toString());
                }

                sleep(500);
            }
        }
    }
}
