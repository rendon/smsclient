package com.inforscience.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

class NewSmsPanel extends JPanel implements Runnable {
    private static final int CHARS_PER_MESSAGE = 160;

    private JTextField numberText;
    private ContactChooser contactChooser;
    private JTextArea messageArea;
    private JButton sendButton;
    private JLabel charsCountLabel;

    private SmsClient smsClient;
    private SentMessagePanel sentMessagePanel;
    private StatusBar statusBar;

    private Color defaultBackground;

    public NewSmsPanel(SmsClient client, SentMessagePanel smp, StatusBar bar)
    {
        smsClient = client;
        sentMessagePanel = smp;
        statusBar = bar;

        setLayout(new BorderLayout());

        JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 5));

        numberText = new JTextField(26);
        defaultBackground = numberText.getBackground();
        numberText.addKeyListener(new ActionHandler());

        JButton chooseContact = new JButton("...");
        chooseContact.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event)
            {
                if (contactChooser != null)
                    contactChooser.setVisible(true);
            }
        });

        northPanel.add(numberText);
        northPanel.add(chooseContact);

        add(northPanel, BorderLayout.NORTH);

        messageArea = new JTextArea(18, 32);
        messageArea.setLineWrap(true);
        messageArea.addKeyListener(new ActionHandler());
        messageArea.setFont(new Font("DialogInput", Font.BOLD, 18));
        add(messageArea, BorderLayout.CENTER);

        JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT, 15, 5));

        charsCountLabel = new JLabel("0 de 1");
        southPanel.add(charsCountLabel);

        sendButton = new JButton("Enviar");
        sendButton.addActionListener(new ActionHandler());
        sendButton.setEnabled(false);
        southPanel.add(sendButton);

        add(southPanel, BorderLayout.SOUTH);
    }

    private class ActionHandler implements KeyListener, ActionListener {
        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
        }

        @Override
        public void keyReleased(KeyEvent event)
        {
            if (event.getSource() == messageArea ||
                    event.getSource() == numberText) {

                String number = numberText.getText();
                String message = messageArea.getText();
                if (number.matches("\\d{10,}") && !message.isEmpty()) {
                    sendButton.setEnabled(true);
                } else {
                    sendButton.setEnabled(false);
                }
            }
            if (event.getSource() == messageArea) {
                int chars = messageArea.getText().length();
                int messages = chars / CHARS_PER_MESSAGE + 1;
                chars %= CHARS_PER_MESSAGE;
                charsCountLabel.setText(chars + " / " + messages);

            } else if (event.getSource() == numberText) {
                if (numberText.getText().matches("\\d*")) {
                    numberText.setBackground(defaultBackground);
                } else {
                    numberText.setBackground(Client.GUI_ERROR_COLOR);
                }
            }

        }

        @Override
        public void actionPerformed(ActionEvent event)
        {
            if (event.getSource() == sendButton) {
                String number = numberText.getText();
                String message = messageArea.getText();
                try {
                    if (smsClient.sendSMS(number, message)) {
                        sentMessagePanel.addMessage(number, message);
                        statusBar.setStatus("Enviado", StatusBar.CODE_SUCCESS);
                        messageArea.setText("");
                        numberText.setText("");
                    } else {
                        statusBar.setStatus("No se pudo enviar",
                                StatusBar.CODE_FAILURE);
                    }
                } catch (Exception e) {
                    statusBar.setForeground(Client.GUI_ERROR_COLOR);
                    statusBar.setStatus("No se pudo enviar",
                            StatusBar.CODE_FAILURE);
                }

            }
        }
    }

    @Override
    public void run()
    {
        Vector<Contact> contacts = smsClient.getContactList();
        contactChooser = new ContactChooser(
                new String[]{"Intentando contactar al servidor..."},
                new String[]{}
        );

        while (true) {
            boolean ok = smsClient.testConnection();
            if (ok)
                contacts = smsClient.getContactList();

            boolean visible = contactChooser.isVisible();
            if (ok && contacts != null) {
                contactChooser.dispose();
                String[] list = new String[contacts.size()];
                String[] numbers = new String[contacts.size()];
                for (int i = 0; i < contacts.size(); i++) {
                    String number = contacts.get(i).getNumber();
                    String name = contacts.get(i).getName();
                    numbers[i] = number;

                    // A simple hack :(
                    while (number.length() < 12)
                        number = " " + number;

                    list[i] = "[" + number + "]  ";
                    list[i] += name;
                }

                contactChooser = new ContactChooser(list, numbers);
                if (visible)
                    contactChooser.setVisible(true);
                break;
            }

            sleep(2000);
        }

    }

    private void sleep(long milliseconds)
    {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    private class ContactList extends JList {
        private String[] numbers;

        public ContactList(String[] items, String[] numbers)
        {
            super(items);
            this.numbers = numbers;
            setFont(new Font("Monospaced", Font.BOLD, 12));
        }

        public String getNumber(int i)
        {
            return numbers[i];
        }
    }

    private class ContactChooser extends JFrame {
        private ContactList contactList;

        public ContactChooser(String[] contacts, String[] numbers)
        {
            setSize(300, 400);
            setLocationRelativeTo(null);
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

            contactList = new ContactList(contacts, numbers);
            contactList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            JScrollPane scrollPane = new JScrollPane(contactList);
            add(scrollPane, BorderLayout.CENTER);

            contactList.updateUI();
            JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            JButton cancelButton = new JButton("Cancelar");
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    contactList.setSelectedIndex(-1);
                    dispose();
                }
            });

            JButton acceptButton = new JButton("Aceptar");
            acceptButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    int index = contactList.getSelectedIndex();
                    try {
                        String number = contactList.getNumber(index);
                        numberText.setText(number);
                        dispose();
                    } catch (IndexOutOfBoundsException e) {
                    }
                }
            });

            southPanel.add(cancelButton);
            southPanel.add(acceptButton);
            add(southPanel, BorderLayout.SOUTH);
        }
    }

}

