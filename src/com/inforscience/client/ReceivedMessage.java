package com.inforscience.client;

public class ReceivedMessage {
    private String transmitter;
    private String text;

    public ReceivedMessage()
    {
        transmitter = "";
        text = "";
    }

    public ReceivedMessage(String transmitter, String text)
    {
        this.transmitter = transmitter;
        this.text = text;
    }

    public String getTransmitter()
    {
        return transmitter;
    }

    public String getText()
    {
        return text;
    }
}
