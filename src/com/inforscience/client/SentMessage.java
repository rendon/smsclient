package com.inforscience.client;

public class SentMessage {
    private String receiver;
    private String text;

    public SentMessage()
    {
        receiver = "";
        text = "";
    }

    public SentMessage(String receiver, String text)
    {
        this.receiver = receiver;
        this.text = text;
    }

    public String getReceiver()
    {
        return receiver;
    }

    public String getText()
    {
        return text;
    }
}

