package com.inforscience.client;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Vector;

class SentMessagePanel extends JPanel implements Runnable {

    private JEditorPane messagePane;
    private SmsClient smsClient;
    private String htmlStyle;
    private String divHeaderStyle;
    private String divMessageStyle;

    private Vector<SentMessage> sentMessages;
    private StatusBar statusBar;
    private HashMap<String, String> contactsMap;

    public SentMessagePanel(SmsClient client, StatusBar bar)
    {
        smsClient = client;
        statusBar = bar;

        setLayout(new BorderLayout());

        messagePane = new JEditorPane();
        messagePane.setEditable(false);
        htmlStyle = "<html><body style=\"background-color: #1C1C1C;\">";
        divHeaderStyle = "<div style=\"padding: 5px; color: white; \">";
        divMessageStyle = "<div style=\"margin-top: 5px; padding: 5px; " +
                "color: white; background-color: #2E7EC4;\">";

        messagePane.setContentType("text/html");
        messagePane.setText(htmlStyle + "</body></html>");
        JScrollPane scrollPane = new JScrollPane(messagePane);
        add(scrollPane, BorderLayout.CENTER);
    }

    private String formatMessage(String to, String message)
    {
        String div = "<div style=\"margin: 5px; padding: 10px;" +
                " background-color: #3C3F41;\">";
        if (contactsMap.containsKey(to))
            to = contactsMap.get(to);
        div += divHeaderStyle + "<strong>Para: </strong>" + to + "</div>";
        div += divMessageStyle + message + "</div>";
        div += "</div>";

        return div;
    }

    public void scrollToBottom()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                int length = messagePane.getDocument().getLength();
                if (length > 1)
                    messagePane.setCaretPosition(length - 1);
            }
        });
    }

    void formatAllMessages()
    {
        String html = htmlStyle;
        for (int i = 0; i < sentMessages.size(); i++) {
            String from = sentMessages.get(i).getReceiver();
            String message = sentMessages.get(i).getText();
            String item = formatMessage(from, message);
            html += item;
        }
        html += "</body></html>";

        messagePane.setText(html);
        scrollToBottom();
    }

    public void addMessage(String to, String message)
    {
        sentMessages.add(new SentMessage(to, message));
        formatAllMessages();
    }

    private void loadContacts()
    {
        Vector<Contact> c = smsClient.getContactList();
        contactsMap = new HashMap<String, String>();

        for (int i = 0; i < c.size(); i++)
            contactsMap.put(c.get(i).getNumber(), c.get(i).getName());
    }

    @Override
    public void run()
    {
        while (true) {
            if (smsClient.testConnection()) {
                sentMessages = smsClient.getSentMessages();
                if (sentMessages != null) {
                    loadContacts();
                    formatAllMessages();
                    messagePane.updateUI();
                    break;
                }
            } else {
                messagePane.setText("<h1>Intentando contactar " +
                        "al servidor...</h1>");
            }
            sleep(2000);
        }

    }

    private void sleep(int milliseconds)
    {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
