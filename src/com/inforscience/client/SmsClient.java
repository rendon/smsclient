package com.inforscience.client;

import java.io.*;
import java.net.*;
import java.util.Vector;

class SmsClient {
    public static final String COMMAND_GET_CONTACTS = "get_contacts";
    public static final String COMMAND_SEND_MESSAGE = "send_message";
    public static final String COMMAND_GET_LAST_MESSAGE = "get_last_message";
    public static final String CODE_TRUE = "TRUE";
    public static final String CODE_FALSE = "FALSE";

    public static final
    String COMMAND_GET_SENT_MESSAGES = "get_sent_messages";

    public static final
    String COMMAND_GET_RECEIVED_MESSAGES = "get_received_messages";

    public static final
    String COMMAND_CHECK_FOR_NEW_MESSAGE = "check_for_new_message";

    public static final String STATUS_OK = "OK";
    public static final String STATUS_FAIL = "FAIL";

    public static final String FIELD_SEPARATOR = String.valueOf((char) 31);
    public static final String RECORD_SEPARATOR = String.valueOf((char) 30);

    public static final int HTTP_STATUS_OK = 200;

    private String serverName;
    private String protocol;
    private int port;

    public SmsClient(String protocol, String serverName, int port)
    {
        setProtocol(protocol);
        setServerName(serverName);
        setPort(port);
    }

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getProtocol()
    {
        return protocol;
    }

    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public boolean testConnection()
    {
        String parameters = "command=" + COMMAND_CHECK_FOR_NEW_MESSAGE;
        try {
            String server = getProtocol() + "://" +
                            getServerName() + ":" +
                            getPort();

            URL url = new URL(server + "/?" + parameters);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            return http.getResponseCode() == HTTP_STATUS_OK;
        } catch (IOException ioe) {
            System.out.println(ioe.toString());
        }

        return false;
    }

    public Vector<SentMessage> getSentMessages()
    {
        Vector<SentMessage> sentMessages = new Vector<SentMessage>();
        String parameters = "command=" + COMMAND_GET_SENT_MESSAGES;

        try {
            String server = getProtocol() + "://" +
                            getServerName() + ":" +
                            getPort();

            URL url = new URL(server + "/?" + parameters);
            System.out.println(url.toString());

            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(http.getInputStream());
            BufferedReader reader = new BufferedReader(in);


            String line;
            while ((line = reader.readLine()) != null) {
                String[] rows = line.split(RECORD_SEPARATOR);
                for (int i = 0; i < rows.length; i++) {
                    String fields[] = rows[i].split(FIELD_SEPARATOR);
                    SentMessage sm = new SentMessage(fields[0], fields[1]);
                    sentMessages.add(sm);
                }
            }

        } catch (IOException ioe) {
            System.out.println(ioe.toString());
            return null;
        }

        return sentMessages;
    }

    public Vector<ReceivedMessage> getReceivedMessages()
    {
        Vector<ReceivedMessage> receivedMessages = new Vector<ReceivedMessage>();
        String parameters = "command=" + COMMAND_GET_RECEIVED_MESSAGES;

        try {
            String server = getProtocol() + "://" +
                            getServerName() + ":" +
                            getPort();

            URL url = new URL(server + "/?" + parameters);
            System.out.println(url.toString());

            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            InputStreamReader in = new InputStreamReader(http.getInputStream());
            BufferedReader reader = new BufferedReader(in);

            String line;
            while ((line = reader.readLine()) != null) {
                String[] rows = line.split(RECORD_SEPARATOR);
                for (int i = 0; i < rows.length; i++) {
                    String fields[] = rows[i].split(FIELD_SEPARATOR);
                    ReceivedMessage rm = new ReceivedMessage(fields[0], fields[1]);
                    receivedMessages.add(rm);
                }
            }

        } catch (IOException ioe) {
            System.out.println(ioe.toString());
            return null;
        }

        return receivedMessages;
    }

    public Vector<Contact> getContactList()
    {
        Vector<Contact> contacts = new Vector<Contact>();
        String command = COMMAND_GET_CONTACTS;
        String parameters = "command=" + command;

        try {
            String server = getProtocol() + "://" +
                            getServerName() + ":" +
                            getPort();

            URL url = new URL(server + "/?" + parameters);
            System.out.println(url.toString());

            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(http.getInputStream());
            BufferedReader reader = new BufferedReader(in);

            String line;
            while ((line = reader.readLine()) != null) {
                String[] rows = line.split(RECORD_SEPARATOR);
                for (int i = 0; i < rows.length; i++) {
                    String fields[] = rows[i].split(FIELD_SEPARATOR);
                    Contact c = new Contact(fields[0], fields[1]);
                    contacts.add(c);
                }
            }

        } catch (IOException ioe) {
            System.out.println(ioe.toString());
            return null;
        }

        return contacts;
    }

    public boolean sendSMS(String number, String message) throws Exception
    {
        String command = COMMAND_SEND_MESSAGE;
        message = URLEncoder.encode(message, "UTF8");
        String parameters = "command=" + command + "&" +
                "number=" + number + "&" +
                "text=" + message;

        try {
            String server = getProtocol() + "://" +
                            getServerName() + ":" +
                            getPort();

            URL url = new URL(server + "/?" + parameters);
            System.out.println(url.toString());

            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(http.getInputStream());
            BufferedReader reader = new BufferedReader(in);
            String ans = reader.readLine();

            return ans.equals(STATUS_OK);

        } catch (IOException ioe) {
            System.out.println(ioe.toString());
        }

        return false; // Fail
    }
}

