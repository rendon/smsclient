package com.inforscience.client;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Color;

class StatusBar extends JPanel implements Runnable {
    public static final int CODE_SUCCESS = 1;
    public static final int CODE_FAILURE = 2;
    private JLabel status;
    private boolean newStatus;

    public StatusBar()
    {
        super();
        super.setPreferredSize(new Dimension(100, 25));
        setLayout(new FlowLayout(FlowLayout.LEFT));
        status = new JLabel("Listo.");
        add(status);
        newStatus = false;
    }

    public void setStatus(String status)
    {
        newStatus = true;
        this.status.setText(status + " ");
    }

    public void setStatus(String status, int code)
    {
        newStatus = true;
        if (code == CODE_SUCCESS) {
            this.status.setText("EXITO: " + status);
        } else if (code == CODE_FAILURE) {
            this.status.setText("FALLO: " + status);
        }

    }

    @Override
    public void run()
    {
        while (true) {
            sleep(500);
            if (newStatus) {
                sleep(3000);
                setStatus(":");
            }
        }
    }

    private void sleep(long milliseconds)
    {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
